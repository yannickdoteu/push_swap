/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/11 15:54:32 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/18 12:49:52 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "../includes/push_swap.h"

void	ft_pushlowest(t_list **a, t_list **b)
{
	int	alen;
	int	i;

	alen = ft_lstlen(*a);
	i = ft_getindex(ft_lowest(*a, AFTER_SIMPLIFICATION), *a);
	if (i > alen / 2)
	{
		while (i < alen)
		{
			ft_reverse_rotate(a);
			write(1, "rra\n", 4);
			i++;
		}
	}
	else
	{
		while (i > 0)
		{
			ft_rotate(a);
			write(1, "ra\n", 3);
			i--;
		}
	}
	ft_push(a, b);
	write(1, "pb\n", 3);
}

void	ft_algo5(t_list *a)
{
	t_list	*b;
	int		blen;

	b = NULL;
	blen = 0;
	while (!ft_is_sorted(a))
	{
		ft_pushlowest(&a, &b);
		blen++;
		if (a->prev->prev == NULL && a->key > a->prev->key)
		{
			ft_swap(&a);
			write(1, "sa\n", 3);
		}
	}
	while (blen > 0)
	{
		ft_push(&b, &a);
		write(1, "pa\n", 3);
		blen--;
	}
	ft_lstfree(a);
	exit(0);
}
