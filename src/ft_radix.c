/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_radix.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/16 11:26:21 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/16 12:06:45 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "../includes/push_swap.h"

void	ft_pa(t_list **a, t_list **b)
{
	ft_printf("pa\n");
	ft_push(b, a);
}

void	ft_sort_one(t_list **a, t_list **b, int i)
{
	if ((*a)->key & (0x1 << i))
	{
		ft_printf("ra\n");
		ft_rotate(a);
	}
	else
	{
		ft_printf("pb\n");
		ft_push(a, b);
	}
}

void	ft_radix(t_list *a)
{
	t_list	*b;
	int		i;
	int		j;
	int		alen;

	b = NULL;
	i = 0;
	alen = ft_lstlen(a);
	while (i < 32)
	{
		j = 0;
		if (ft_is_sorted(a))
			break ;
		while (j < alen)
		{
			ft_sort_one(&a, &b, i);
			j++;
		}
		while (b != NULL)
			ft_pa(&a, &b);
		i++;
	}
	ft_lstfree(a);
	exit(0);
}
