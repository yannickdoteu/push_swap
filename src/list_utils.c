/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/06 12:02:43 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/16 11:07:40 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_print_list(t_list *lst)
{
	while (lst != NULL)
	{
		ft_printf("%i\t", lst->key);
		lst = lst->prev;
	}
}

void	ft_lstfree(t_list *lst)
{
	t_list	*prev;

	while (lst != NULL)
	{
		prev = lst->prev;
		free(lst);
		lst = prev;
	}
}

int	ft_lstlen(t_list *lst)
{
	int	i;

	i = 0;
	while (lst != NULL)
	{
		i++;
		lst = lst->prev;
	}
	return (i);
}

int	ft_is_sorted(t_list	*lst)
{
	if (lst == NULL)
	{
		ft_printf("ERROR: ft_is_sorted is called with NULL\n");
		return (0);
	}
	while (lst->prev != NULL)
	{
		if (lst->key > lst->prev->key)
			return (0);
		lst = lst->prev;
	}
	return (1);
}
