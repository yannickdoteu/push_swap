/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/06 18:37:46 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/16 11:52:29 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	main(int argc, char *argv[])
{
	t_list	*a;

	if (argc < 2)
		return (0);
	if (!ft_is_numbers_no_doubles(argc, argv))
		ft_abort(NULL, "Error\n");
	a = ft_check_and_create_lst(argc, argv);
	ft_simplify(a);
	if (argc == 4)
		ft_algo3(a);
	else if (argc >= 5 && argc <= 15)
		ft_algo5(a);
	else
		ft_radix(a);
}
