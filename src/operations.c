/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/06 12:02:37 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/18 12:50:32 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_swap(t_list **lst)
{
	t_list	*first;
	t_list	*second;
	t_list	*third;

	if (lst == NULL || *lst == NULL)
		write(2, "ft_swap wrong input aah\n", 24);
	first = (*lst)->prev;
	second = *lst;
	third = (*lst)->prev->prev;
	*lst = first;
	first->prev = second;
	second->prev = third;
}

void	ft_push(t_list **a, t_list **b)
{
	t_list	*transfer;

	if (a == NULL || b == NULL)
		return ;
	transfer = *a;
	*a = (*a)->prev;
	transfer->prev = *b;
	*b = transfer;
}

void	ft_rotate(t_list **lst)
{
	t_list	*bottom;
	t_list	*transfer;

	if (*lst == NULL || (*lst)->prev == NULL)
		return ;
	bottom = *lst;
	while (bottom->prev != NULL)
		bottom = bottom->prev;
	transfer = *lst;
	bottom->prev = transfer;
	*lst = (*lst)->prev;
	transfer->prev = NULL;
}

void	ft_reverse_rotate(t_list **lst)
{
	t_list	*bottom;
	t_list	*prebottom;

	if (*lst == NULL || (*lst)->prev == NULL)
	{
		ft_printf("Error: ft_reverse_rotate called with NULL or senseless\n");
		exit(-1);
	}
	prebottom = *lst;
	while (prebottom->prev->prev != NULL)
		prebottom = prebottom->prev;
	bottom = prebottom->prev;
	bottom->prev = *lst;
	prebottom->prev = NULL;
	*lst = bottom;
}
