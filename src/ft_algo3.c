/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/11 13:12:30 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/11 17:44:47 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_algo3(t_list *a)
{
	int	array[3];
	int	i;

	i = 0;
	while (a != NULL)
	{
		array[i] = a->key;
		a = a->prev;
		i++;
	}
	if (array[0] == 0 && array[1] == 2)
		write(1, "rra\nsa\n", 7);
	if (array[0] == 1 && array[1] == 0)
		write(1, "sa\n", 3);
	if (array[0] == 1 && array[1] == 2)
		write(1, "rra\n", 4);
	if (array[0] == 2 && array[1] == 0)
		write(1, "ra\n", 3);
	if (array[0] == 2 && array[1] == 1)
		write(1, "ra\nsa\n", 6);
	ft_lstfree(a);
	exit(0);
}
