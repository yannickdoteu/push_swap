/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/13 16:33:44 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/18 14:00:35 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int	ft_convert(int i, int sign, const char *str, t_list *lst)
{
	unsigned long int	result;

	result = 0;
	while (str[i] == '0')
		i++;
	while (ft_isdigit(str[i]) == 1)
	{
		result = (result * 10) + (str[i] - 48);
		if ((result > (unsigned long int)INT_MIN * -1) && sign == -1)
			ft_abort(lst, "Error\n");
		if ((result > (unsigned long int)INT_MAX) && sign == 1)
			ft_abort(lst, "Error\n");
		i++;
	}
	return ((int)result);
}

int	ft_atoi(const char *str, t_list *lst)
{
	int	sign;
	int	base;
	int	i;

	i = 0;
	base = 0;
	sign = 1;
	if (!(*str))
		ft_abort(lst, "Error\n");
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			sign *= -1;
		i++;
	}
	if (ft_isdigit(str[i]) == 1)
	{
		base = ft_convert(i, sign, str, lst);
		return (base * sign);
	}
	else
		return (0);
}
