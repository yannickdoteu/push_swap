/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 11:44:43 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/16 11:15:42 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_abort(t_list *lst, char *error)
{
	ft_lstfree(lst);
	ft_printf(error);
	exit(-1);
}

int	ft_is_numbers_no_doubles(int argc, char *argv[])
{
	int	i;
	int	j;
	int	k;
	int	current;

	i = 1;
	while (i < argc)
	{
		j = -1;
		while (argv[i][++j] != '\0')
		{
			if (argv[i][j] < '0' || argv[i][j] > '9')
				if (!(argv[i][j] == '-' && j == 0))
					return (0);
		}
		k = 1;
		current = ft_atoi(argv[i], NULL);
		while (k < i)
		{
			if (ft_atoi(argv[k++], NULL) == current)
				return (0);
		}
		i++;
	}
	return (1);
}

t_list	*ft_check_and_create_lst(int argc, char *argv[])
{
	int		i;
	t_list	*new;
	t_list	*lst;

	i = argc - 1;
	lst = NULL;
	while (i > 0)
	{
		new = malloc(sizeof(*new));
		if (new == NULL)
			exit(-1);
		new->simplified = 0;
		new->key = ft_atoi(argv[i], lst);
		if (lst == NULL)
			new->prev = NULL;
		else
			new->prev = lst;
		lst = new;
		i--;
	}
	return (lst);
}
