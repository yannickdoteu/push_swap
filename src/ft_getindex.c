/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getindex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/11 15:26:02 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/11 15:53:53 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	ft_getindex(t_list *needle, t_list *haystack)
{
	int	i;

	i = 0;
	while (haystack != NULL)
	{
		if (needle == haystack)
			return (i);
		haystack = haystack->prev;
		i++;
	}
	return (-1);
}
