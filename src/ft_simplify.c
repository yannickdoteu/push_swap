/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_simplify.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/06 12:09:26 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/11 19:19:33 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

t_list	*ft_lowest(t_list *lst, int mode)
{
	t_list	*lowest;

	lowest = NULL;
	while (lst != NULL)
	{
		if (!(lst->simplified) || mode == AFTER_SIMPLIFICATION)
		{
			if (lowest == NULL || lst->key < lowest->key)
				lowest = lst;
		}
		lst = lst->prev;
	}
	return (lowest);
}

void	ft_simplify(t_list *lst)
{
	int				i;
	t_list			*lowest;

	i = 0;
	while (i < ft_lstlen(lst))
	{
		lowest = ft_lowest(lst, DURING_SIMPLIFICATION);
		if (lowest == NULL)
			return ;
		lowest->key = i;
		lowest->simplified = 1;
		i++;
	}
}
