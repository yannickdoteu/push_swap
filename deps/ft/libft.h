/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/13 16:33:23 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/10 18:18:28 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <unistd.h>
# include <limits.h>
# include <stdarg.h>

/*
** PART ONE
*/
int		ft_isdigit(char c);
size_t	ft_strlen(const char *str);

/*
** PRINTF
*/
void	ft_set2zero(int *one, int *two);
void	ft_set3zero(int *one, int *two, int *three);
void	ft_putul_base(unsigned long int nb, char *base, int *count);
void	ft_putstr(char *s, int *count);
void	ft_putint_base(int nbr, char *base, int *count);
int		ft_base_is_valid(char *base);
int		ft_charstr(char c, char *set);
void	ft_putchar(char c, int *count);
int		ft_printf(const char *s, ...);
void	ft_putuint_base(unsigned int nbr, char *base, int *count);

#endif
