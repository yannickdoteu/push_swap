# push_swap

A [42](https://42network.org) project about sorting algorithms.

## Usage

After cloning do:
```bash
make
./push_swap -123 314 239 # any list of unsorted integers will work
```

For more fun, use the fabulous [push_swap visualizer](https://github.com/o-reo/push_swap_visualizer) (see [requirements](https://github.com/o-reo/push_swap_visualizer?tab=readme-ov-file#install)):

When inside this repository, do the following to open the visualizer:
```bash
git clone https://github.com/o-reo/push_swap_visualizer
cd push_swap_visualizer
mkdir build
cd build
cmake ..
make
./bin/visualizer
```

## Learn more

Learn more on what this project is about and what I learned from it on my blog: https://yannick.eu/push_swap
