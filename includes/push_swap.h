/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 11:40:01 by yaretel-          #+#    #+#             */
/*   Updated: 2023/02/18 12:51:46 by yaretel-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# include "../deps/ft/libft.h"
# define AFTER_SIMPLIFICATION 1
# define DURING_SIMPLIFICATION 0

typedef struct s_list
{
	int				key;
	int				simplified;
	struct s_list	*prev;
}					t_list;

t_list	*ft_check_and_fill_a(int argc, char *argv[]);
int		ft_atoi(const char *str, t_list *lst);
void	ft_abort(t_list *lst, char *error);
int		ft_is_numbers_no_doubles(int argc, char *argv[]);
t_list	*ft_check_and_create_lst(int argc, char *argv[]);
void	ft_rotate(t_list **lst);
void	ft_reverse_rotate(t_list **lst);
void	ft_swap(t_list **lst);
void	ft_push(t_list **a, t_list **b);
void	ft_print_list(t_list *lst);
void	ft_lstfree(t_list *lst);
int		ft_lstlen(t_list *lst);
int		ft_is_sorted(t_list	*lst);
void	ft_check_doubles(t_list *lst);
t_list	*ft_lowest(t_list *lst, int mode);
void	ft_simplify(t_list *lst);
int		ft_getindex(t_list *needle, t_list *haystack);
void	ft_algo3(t_list *lst);
void	ft_algo5(t_list *lst);
void	ft_radix(t_list *a);

#endif
