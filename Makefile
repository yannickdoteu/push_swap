# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: yaretel- <yaretel-@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/01/04 20:29:18 by yaretel-          #+#    #+#              #
#    Updated: 2023/02/16 11:53:07 by yaretel-         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PROJECT =		push_swap
NAME =			$(PROJECT)
HEADER =		includes/$(PROJECT).h
LIB =			lib$(PROJECT).a
DEPS_NAME =		ft
DEPS =			$(foreach dep, $(DEPS_NAME), deps/$(dep)/lib$(dep).a)
SRCS =			push_swap\
				list_utils\
				operations\
				ft_simplify\
				ft_atoi\
				ft_algo3\
				ft_algo5\
				ft_getindex\
				ft_radix
SRC_MAIN = 		main
OBJS =			$(addprefix obj/, $(addsuffix .o, $(SRCS)))
OBJS_MAIN =		$(addprefix obj/, $(addsuffix .o, $(SRC_MAIN)))
CFLAGS =		
FTFLAGS = 		-Wall -Wextra -Werror
FLAGS =			$(CFLAGS) $(FTFLAGS)

all: 		$(NAME)

$(NAME): $(DEPS) $(OBJS) $(OBJS_MAIN)
	$(CC) $(FLAGS) -o $(NAME) $(OBJS) $(OBJS_MAIN) $(DEPS)

obj/%.o: src/%.c $(HEADER)
	mkdir -p $(dir $@)
	$(CC) -c $(FLAGS) -o $@ $<

$(DEPS):
	make -C $(dir $@) FLAGS='$(FLAGS)'

lib: $(LIB)

$(LIB): $(DEPS) $(OBJS)
	ar -rcs $(LIB) $(OBJS) $(dir $(DEPS))*.o

clean:
	make $@ -C $(dir $(DEPS))
	rm -rf $(dir $(OBJS))

fclean: clean
	make $@ -C $(dir $(DEPS))
	rm -rf $(NAME)
	rm -rf $(LIB)

re: fclean all

debug:
	$(MAKE) CFLAGS="-g"

.PHONY: re all clean fclean debug lib
